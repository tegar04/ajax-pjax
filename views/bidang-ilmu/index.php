<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $searchModel app\models\BidangIlmuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bidang Ilmus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bidang-ilmu-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Bidang Ilmu', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    Pjax::begin(); 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',
            'deskripsi:ntext',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php 
    Pjax::end(); 
    ?>

</div>
