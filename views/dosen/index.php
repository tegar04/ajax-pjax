<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Dosens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dosen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Dosen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    Pjax::begin(); 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nidn',
            'nama',
            'gelar_belakang',
            'gelar_depan',
            //'jk',
            //'tmp_lahir',
            //'tgl_lahir',
            //'alamat',
            //'email:email',
            //'thn_masuk',
            //'prodi_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php 
    Pjax::end(); 
    ?>

</div>
