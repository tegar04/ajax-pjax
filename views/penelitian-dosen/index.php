<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PenelitianDosenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Penelitian Dosens';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="penelitian-dosen-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?= Html::a('Create Penelitian Dosen', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
    Pjax::begin(); 
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'dosen_id',
            'judul:ntext',
            'mulai',
            'akhir',
            //'tahun_ajaran',
            //'tim_riset',
            //'bidang_ilmu_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php 
    Pjax::end(); 
    ?>

</div>
