<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Category', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php \yii\widgets\Pjax::begin(['timeout'=>5000,'id'=>'pjax-gridview']); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'description',
            'created_at',
            //'updated_at',
            'created_by',
            //'updated_by',

            ['class' => 'yii\grid\ActionColumn'],

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        $icon='<span class="glyphicon glyphicon-eye-open"></span>';
                        return Html::a($icon,$url);
                    },
                    'update' => function ($url, $model) {
                        $icon='<span class="glyphicon glyphicon-pencil"></span>';
                        return Html::a($icon,$url);
                    },
                    'delete' => function ($url, $model) {
                        $icon='<span class="glyphicon glyphicon-trash"></span>';
                        return Html::a($icon,$url,[
                            'class'=>'pjaxDelete',
                            'data-confirm'=>"Are you sure you want to delete this item?",
                            'data-method'=>'post',
                        ]); 
                    },   
                ]
            ],
        ],
   ]); ?>
    <?php \yii\widgets\Pjax::end() ?>

</div>
